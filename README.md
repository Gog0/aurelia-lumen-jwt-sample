## Synopsis

This is an example of how to create an [Aurelia](http://aurelia.io) App using a [Lumen](https://lumen.laravel.com) backend, the whole thing using a [JWT](https://jwt.io/introduction/) authentication system. Could be a good start for your own projects.  

## Read more

This project is part of a series of articles that I wrote on my blog. So feel free to read the [PART1 - Aurelia](http://shortcodestories.com/blog/build-aurelia-app-with-lumen-api-and-jwt) and [PART2 - Lumen](http://shortcodestories.com/blog/build-aurelia-app-with-lumen-api-and-jwt-2)!