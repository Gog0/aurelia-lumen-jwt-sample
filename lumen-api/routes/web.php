<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->post('/auth/login', 'AuthController@postLogin');

$app->group(['prefix' => 'api/protected',
            'namespace' => 'App\Http\Controllers\Api',
            'middleware' => 'auth'],
    function () use ($app) {
        $app->get('/', 'ProtectedController@getProtected');
});

$app->group(['prefix' => 'api/public', 'namespace' => 'App\Http\Controllers\Api'], function () use ($app) {
    $app->get('/', 'PublicController@getPublic');
});