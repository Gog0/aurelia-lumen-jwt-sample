<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    protected $guard = "api";

    public function postLogin(Request $request)
    {
        $this->validate($request, [
            'email'    => 'required|email',
            'password' => 'required',
        ]);


        // attempt to verify the credentials and create a token for the user
        if (! $token = app('auth')->guard($this->guard)->attempt($request->only('email', 'password'))) {
            return response()->json(['error' => 'invalid_credentials'], 401);
        }

        // all good so return the token
        return response()->json(compact('token'));
    }
}