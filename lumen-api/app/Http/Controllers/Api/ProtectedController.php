<?php

namespace App\Http\Controllers\Api;

use Laravel\Lumen\Routing\Controller as BaseController;

class ProtectedController extends BaseController
{
    public function getProtected() {
        return response()->json(['message' => 'I am a protected message']);
    }
}
