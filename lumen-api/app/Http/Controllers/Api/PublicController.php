<?php

namespace App\Http\Controllers\Api;

use Laravel\Lumen\Routing\Controller as BaseController;

class PublicController extends BaseController
{
    public function getPublic() {
        return response()->json(['message' => 'I am a public message']);
    }
}
