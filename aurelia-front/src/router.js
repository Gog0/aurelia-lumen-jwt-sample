import {AuthorizeStep} from 'aurelia-authentication';

export default function(config){
    config.title = 'Aurelia';
    config.options.pushState = true;
    config.options.root = '/';
    config.addPipelineStep('authorize', AuthorizeStep); // Add a route filter to the authorize extensibility point.

    config.map([
        { route: '',              moduleId: './welcome',         nav: true, title:'Welcome' },
        { route: 'login',         moduleId: './auth/login',      nav: false, title:'Login' },
        { route: 'not-protected', moduleId: './not-protected',   nav: true, title:'Not Protected' },
        { route: 'protected',     moduleId: './protected',       nav: true, title:'Protected', auth:true },
    ]);
};