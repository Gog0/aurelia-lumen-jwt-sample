import extend from 'extend';

var baseConfig = {
    endpoint: 'auth',
    configureEndpoints: ['auth', 'api'],
    loginRedirect: '/',
    logoutRedirect: '/'
};

var configForDevelopment = {
    providers: {
    }
};

var configForProduction = {
    providers: {
    }
};

var config;
if (window.location.hostname === 'localhost') {
    config = extend(true, {}, baseConfig, configForDevelopment);
}
else {
  config = extend(true, {}, baseConfig, configForProduction);
}

export default config;