import {inject} from 'aurelia-framework';
import {Router} from 'aurelia-router';
import appRouter from './router';

@inject(Router)
export class App {
  constructor(router) {
    this.router = router;
    this.router.configure(appRouter);
  }
}
