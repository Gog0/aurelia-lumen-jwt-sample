import {AuthService} from 'aurelia-authentication';
import {inject, computedFrom} from 'aurelia-framework';

@inject(AuthService)
export class Login{
    constructor(authService){
        this.authService = authService;
    };
    
    email='';
    password='';

    // use authService.login(credentialsObject) to login to your auth server
    login(email, password) {
        return this.authService.login({email, password});
    };
}