import {inject} from 'aurelia-framework';
import {Endpoint} from 'aurelia-api';

@inject(Endpoint.of('api'))
export class NotProtected {
    constructor(apiEndpoint) {
        this.pageType = 'not a protected page';

        apiEndpoint.find('/protected')
        .then(response => {
            console.log(response.message)
         })
        .catch(error => console.error(error));
        
        apiEndpoint.find('/public')
        .then(response => {
            console.log(response.message)
         })
        .catch(console.error);
    }
}